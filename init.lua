local execute = vim.api.nvim_command
local fn = vim.fn

local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'

if fn.empty(fn.glob(install_path)) > 0 then
  fn.system({'git', 'clone', 'https://github.com/wbthomason/packer.nvim', install_path})
  execute 'packadd packer.nvim'
end

require 'deps'

require 'settings'

require 'config.colors'
require 'config.treesitter'
require 'config.snippets'
require 'config.file-browser'
require 'config.interface'
require 'config.git'
require 'config.typing'

require 'lsp'
require 'java_utils'
