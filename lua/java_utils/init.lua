local M = {}

local function file_exists(name)
	local f = io.open(name, "r")
	if f ~= nil then
		io.close(f)
		return true
	else
		return false
	end
end

local get_test_file = function()
	local bufname = vim.api.nvim_buf_get_name(0);
	if (bufname:find("main/java")) then
		local filename = bufname:match("^.+/(.+)%..+$") .. "Test"
		local test_folder = bufname:match("(.+)/.+%..+$"):gsub(
			"main/java",
			"test/java"
		)
		local test_file = test_folder .. "/" .. filename .. ".java"
		local package = "package " .. bufname:match("java/(.+)/.+%..+$"):gsub(
			"/",
			"."
		) .. ";\n\n"
		if (not file_exists(test_file)) then
			os.execute("mkdir " .. test_folder .. " -p")
			local file = io.open(test_file, "w")
			file:write(package .. "public class " .. filename .. " {\n\n}")
			file:close()
		end
		return test_file
	end
end

M.go_to_test_file = function()
	local test_file = get_test_file();
	if (test_file ~= nil) then
		vim.cmd("edit " .. test_file)
	end
end

M.open_split_test = function()
	local test_file = get_test_file();
	if (test_file ~= nil) then
		vim.cmd("bel vsp " .. test_file)
	end
end

vim.api.nvim_set_keymap('n', 'gt', '<cmd>lua require(\'java_utils\').go_to_test_file()<cr>', {noremap = true})
vim.api.nvim_set_keymap('n', 'gT', '<cmd>lua require(\'java_utils\').open_split_test()<cr>', {noremap = true})

return M;
