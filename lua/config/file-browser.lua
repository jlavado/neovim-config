vim.api.nvim_set_keymap('n', 'ff', '<cmd>lua require("telescope.builtin").find_files()<cr>', {noremap = true})
vim.api.nvim_set_keymap('n', 'fg', '<cmd>lua require("telescope.builtin").live_grep()<cr>', {noremap = true})
vim.api.nvim_set_keymap('n', 'fb', '<cmd>lua require("telescope.builtin").buffers()<cr>', {noremap = true})
vim.api.nvim_set_keymap('n', 'fh', '<cmd>lua require("telescope.builtin").git_bcommits()<cr>', {noremap = true})
vim.api.nvim_set_keymap('n', 'fr', '<cmd>lua require("telescope.builtin").lsp_references()<cr>', {noremap = true})
vim.api.nvim_set_keymap('n', 'fi', '<cmd>lua require("telescope.builtin").lsp_implementations()<cr>', {noremap = true})
vim.api.nvim_set_keymap('n', 'fd', '<cmd>lua require("telescope.builtin").lsp_definitions()<cr>', {noremap = true})
vim.api.nvim_set_keymap('n', 'fa', '<cmd>lua require("telescope.builtin").lsp_code_actions()<cr>', {noremap = true})
vim.api.nvim_set_keymap('v', 'fa', '<cmd>lua require("telescope.builtin").lsp_code_actions()<cr>', {noremap = true})
vim.api.nvim_set_keymap('n', '<F2>', '<cmd>NvimTreeToggle<cr>', {noremap = true})

vim.g.nvim_tree_group_empty = 1

require'nvim-tree'.setup {
    hide_dotfiles = 1,
    update_focused_file = {
        enable      = true,
        update_cwd  = false,
        ignore_list = {}
    },
    view = {
        auto_resize = true,
    }
}

require('telescope').setup {
    defaults = {
        file_ignore_patterns = {'**/target', '*.class'},
        path_display = {'smart'},
        layout_strategy = "vertical",
    }
}

require('telescope').load_extension('fzf')
