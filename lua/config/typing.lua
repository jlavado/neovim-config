--require "surround".setup {}

local cmp = require 'cmp'
local compare = require('cmp.config.compare')

cmp.setup {
    mapping = {
        ['<C-p>'] = cmp.mapping.select_prev_item(),
        ['<C-n>'] = cmp.mapping.select_next_item(),
        ['<C-d>'] = cmp.mapping.scroll_docs(-4),
        ['<C-f>'] = cmp.mapping.scroll_docs(4),
        ['<C-Space>'] = cmp.mapping.complete(),
        ['<C-e>'] = cmp.mapping.close(),
        ['<CR>'] = cmp.mapping.confirm({
            behavior = cmp.ConfirmBehavior.Replace,
            select = true,
        })
    },
    documentation = false,
    sources = {{name = 'nvim_lsp'}, {name = 'buffer'}, {name = 'path'}},
    sorting = {
        priority_weight = 3,
        comparators = {
            compare.score,
            compare.recently_used,
            compare.offset,
            compare.order,
            compare.kind,
            compare.exact,
            compare.sort_text,
            compare.length,
        }
    }
}

vim.cmd("vnoremap ; <Plug>Lightspeed_f")

vim.cmd("inoremap <silent><expr> <C-Space> compe#complete()")
vim.cmd("inoremap <silent><expr> <CR>      compe#confirm('<CR>')")
vim.cmd("inoremap <silent><expr> <C-e>     compe#close('<C-e>')")
vim.cmd("inoremap <silent><expr> <C-f>     compe#scroll({ 'delta': +4 })")
vim.cmd("inoremap <silent><expr> <C-d>     compe#scroll({ 'delta': -4 })")

require('kommentary.config').configure_language("java", {
    prefer_single_line_comments = true,
})

vim.api.nvim_set_keymap("n", "<space>c", "<Plug>kommentary_line_default", {})
vim.api.nvim_set_keymap("v", "<space>c", "<Plug>kommentary_visual_default<C-c>", {})

vim.api.nvim_set_keymap("n", "<leader>cc", "<Plug>kommentary_line_increase", {})
vim.api.nvim_set_keymap("n", "<leader>cc", "<Plug>kommentary_motion_increase", {})
vim.api.nvim_set_keymap("n", "<leader>cd", "<Plug>kommentary_line_decrease", {})
vim.api.nvim_set_keymap("n", "<leader>cd", "<Plug>kommentary_motion_decrease", {})
vim.api.nvim_set_keymap("x", "<leader>cd", "<Plug>kommentary_visual_decrease", {})

