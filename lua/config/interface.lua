require('colorizer').setup()
require("bufferline").setup{}

vim.api.nvim_set_keymap('n', '<space><space>', '<cmd>TZFocus<cr>', {noremap = true})
vim.api.nvim_set_keymap('n', '[[', '<cmd>BufferLineCyclePrev<cr>', {noremap = true})
vim.api.nvim_set_keymap('n', ']]', '<cmd>BufferLineCycleNext<cr>', {noremap = true})
vim.api.nvim_set_keymap('v', '??', '<cmd>lua require("dapui").eval()<cr>', {noremap = true})
vim.api.nvim_set_keymap('n', '<leader>p', '<cmd>Glow<CR>', {noremap = true})

local gl = require('galaxyline')
local colors = require('galaxyline.theme').default
local condition = require('galaxyline.condition')
local gls = gl.section
gl.short_line_list = {'NvimTree', 'vista', 'dbui', 'packer'}

gls.mid[1] = {
	FileName = {
		provider = 'FileName',
		condition = condition.buffer_not_empty,
		separator = ' ',
		highlight = {colors.cyan, nil, 'bold'}
	}
}

gls.mid[2] = {
	DiagnosticError = {
		provider = 'DiagnosticError',
		icon = '  ',
		highlight = {colors.red, nil}
	}
}
gls.mid[3] = {
	DiagnosticWarn = {
		provider = 'DiagnosticWarn',
		icon = '  ',
		highlight = {colors.yellow, nil},
	}
}

gls.mid[4] = {
	DiagnosticHint = {
		provider = 'DiagnosticHint',
		icon = '  ',
		highlight = {colors.cyan, nil},
	}
}

gls.mid[5] = {
	DiagnosticInfo = {
		provider = 'DiagnosticInfo',
		icon = '  ',
		highlight = {colors.blue, nil},
	}
}

gls.right[1] = {
	FileEncode = {
		provider = 'FileEncode',
		condition = condition.hide_in_width,
		separator = ' ',
		separator_highlight = {'NONE', nil},
		highlight = {colors.green, nil, 'bold'}
	}
}
gls.right[2] = {
	FileFormat = {
		provider = 'FileFormat',
		condition = condition.hide_in_width,
		separator = ' ',
		separator_highlight = {'NONE', nil},
		highlight = {colors.green, nil, 'bold'}
	}
}
gls.right[3] = {
	GitIcon = {
		provider = function()
			return '  '
		end,
		condition = condition.check_git_workspace,
		separator = ' ',
		separator_highlight = {'NONE', nil},
		highlight = {colors.violet, nil, 'bold'},
	}
}
gls.right[4] = {
	GitBranch = {
		provider = 'GitBranch',
		condition = condition.check_git_workspace,
		highlight = {colors.violet, nil, 'bold'},
	}
}
gls.right[5] = {
	DiffAdd = {
		provider = 'DiffAdd',
		condition = condition.hide_in_width,
		icon = '  ',
		separator = ' ',
		highlight = {colors.green, nil},
	}
}
gls.right[6] = {
	DiffModified = {
		provider = 'DiffModified',
		condition = condition.hide_in_width,
		icon = ' 柳',
		highlight = {colors.orange, nil},
	}
}
gls.right[7] = {
	DiffRemove = {
		provider = 'DiffRemove',
		condition = condition.hide_in_width,
		icon = '  ',
		highlight = {colors.red, nil},
	}
}
