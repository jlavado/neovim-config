require 'nvim-treesitter.configs'.setup {
    highlight = { enable = true, },
    indent = {enable = false},
    playground = { enable = true }
}

function _G.custom_fold_text()
    local line_count = vim.v.foldend - vim.v.foldstart + 1
    return string.rep(" ",vim.fn.indent(vim.v.foldstart)) .. "... " .. line_count .. " lines"
end

-- vim.o.foldmethod = 'expr'
-- vim.o.foldexpr = 'nvim_treesitter#foldexpr()'
vim.opt.foldtext = 'v:lua.custom_fold_text()'
vim.opt.fillchars = 'fold: '

require("vim.treesitter.query").set_query("java", "imports", "(import_declaration)+ @imports")
