local Color, c, Group, g, styles = require('colorbuddy').setup()

local b = styles.bold
local r = styles.reverse
local i = styles.italic
local ul = styles.underline
local uc = styles.undercurl

Color.new('bg0', '#051720')
Color.new('bg1', '#204565')
Color.new('fg0', '#f2e5bc')
Color.new('fg1', '#ffffec')

Color.new('bg_red', '#220000')
Color.new('bg_orange', '#222200')
Color.new('bg_green', '#002200')

Color.new('scarlet', '#551111')
Color.new('red', '#cc6666')
Color.new('orange', '#de935f')
Color.new('green', '#66ffaa')
Color.new('yellow', '#f8fe7a')
Color.new('blue', '#41aaff')
Color.new('aqua', '#8ec07c')
Color.new('cyan', '#43C5Cf')
Color.new('purple', '#8e6fbd')
Color.new('violet', '#b294bb')
Color.new('brown', '#a3685a')
Color.new('grey', '#777777')
Color.new('discrete', '#507595')

Group.new('Normal', c.fg0, c.bg0)
Group.new('Cursor', c.fg1, c.fg1)
Group.new('CursorIM', nil, nil, r)
Group.new('CursorLine', nil, nil)
Group.new('LineNr', c.bg1, nil)
Group.new('CursorLineNr', c.fg1, nil, b)
Group.new('ColorColumn', c.fg0, c.bg0)
Group.new('SignColumn', c.fg0, c.bg0)
Group.new('StatusLine', c.fg0, c.bg0, ul + b, c.fg0)
Group.new('StatusLineNC', c.fg0, nil, ul + b, c.bg1)
Group.new('Visual', nil, c.bg1)
Group.new('Pmenu', c.fg0, c.bg1)
Group.new('PmenuSel', c.bg1, c.fg0)
Group.new('PmenuSbar', c.fg0, c.bg1)
Group.new('PmenuThumb', c.fg0, c.bg1)
Group.new('MatchParen', c.red, nil, b)
Group.new('Search', nil, nil, r)
Group.new('IncSearch', nil, nil, r)
Group.new('TabLineFill', c.blue, c.bg1)
Group.new('TabLineSel', c.green, c.bg1)
Group.new('TabLine', c.fg1, c.bg1)
Group.new('VertSplit', c.bg1, nil, b)
Group.new('NonText', c.bg1, nil)
Group.new('ModeMsg', c.green, nil, b + r)

Group.new('Error', c.red, nil, b)
Group.new('TSError', c.red, nil, b)
Group.new('LspDiagnosticsDefaultError', c.red, nil, b)

Group.new('LspDiagnosticsSignError', c.red, nil, b)
Group.new('LspDiagnosticsSignWarning', c.yellow, nil, b)
Group.new('LspDiagnosticsSignInformation', c.green, nil, b)
Group.new('LspDiagnosticsSignHint', c.blue, nil, b)

Group.new('VGitSignAdd', c.green, nil)
Group.new('VGitSignChange', c.orange, nil)
Group.new('VGitSignDelete', c.red, nil)
Group.new('VGitViewWordAdd', nil, nil, uc, c.orange)
Group.new('VGitViewWordRemove', nil, nil, uc, c.orange)
Group.new('VGitSignDelete', c.red, nil)
--[[ VGitIndicator
VGitStatus
VGitBorder ]]
Group.new('DiffAdd', nil, c.bg_green)
Group.new('DiffChange', nil, c.bg_orange)
Group.new('DiffDelete', nil, c.bg_red)
Group.new('DiffText', nil, c.bg_orange)

Group.new('Comment', c.grey, nil, i)
Group.new('Folded', c.grey, nil, i)
Group.new('Constant', c.violet)
Group.new('String', c.aqua, nil, i)
Group.new('Character', c.purple)
Group.new('Boolean', c.violet)
Group.new('Float', c.violet)
Group.new('Number', c.violet)
Group.new('Identifier', c.fg0)
Group.new('Function', c.cyan, nil, b)
Group.new('Statement', c.violet)
Group.new('Conditional', c.violet, nil, b)
Group.new('Repeat', c.violet, nil, b)
Group.new('Label', c.violet)
Group.new('Operator', c.orange)
Group.new('Keyword', c.discrete)
Group.new('Exception', c.red)
Group.new('PreProc', c.purple)
Group.new('Include', c.discrete)
Group.new('Define', c.aqua)
Group.new('Macro', c.blue, nil, b)
Group.new('PreCondit', c.purple)
Group.new('Type', c.blue)
Group.new('StorageClass', c.yellow, nil, b)
Group.new('Structure', c.aqua, nil, b)
Group.new('Typedef', c.yellow)
Group.new('Special', c.orange, nil, i)
Group.new('SpecialChar', c.cyan)
Group.new('Tag', c.orange)
Group.new('Delimiter', c.orange)
Group.new('SpecialComment', c.grey)
Group.new('Debug', c.violet)
Group.new('Underlined', c.blue, nil, ul)
Group.new('Ignore', c.grey)
Group.new('Error', c.red, nil, b + r)
Group.new('Todo', c.orange, nil, b + i)

Group.new("TSPunctDelimiter", g.Delimiter)
Group.new("TSPunctBracket", c.fg0)
Group.new("TSPunctSpecial", c.fg0)
Group.new("TSConstant", g.Constant)
Group.new("TSConstBuiltin", g.Constant)
Group.new("TSConstMacro", g.Constant)
Group.new("TSString", g.String)
Group.new("TSStringRegex", c.violet)
Group.new("TSStringEscape", c.violet)
Group.new("TSCharacter", g.Character)
Group.new("TSNumber", g.Number)
Group.new("TSBoolean", g.Boolean)
Group.new("TSFloat", g.Float)
Group.new("TSFunction", g.Function)
Group.new("TSFuncBuiltin", g.Function)
Group.new("TSFuncMacro", g.Function)
Group.new("TSParameter", c.fg1)
Group.new("TSParameterReference", g.TSParameter)
Group.new("TSMethod", g.Function, nil, i)
Group.new("TSField", c.green)
Group.new("TSProperty", g.TSField)
Group.new("TSConstructor", c.purple)
Group.new("TSConditional", g.Conditional)
Group.new("TSRepeat", g.Repeat)
Group.new("TSLabel", g.Label)
Group.new("TSOperator", g.Operator)
Group.new("TSKeyword", g.Keyword)
Group.new("TSKeywordFunction", c.purple)
Group.new("TSException", g.Exception)
Group.new("TSType", g.Type)
Group.new("TSTypeBuiltin", g.Type)
Group.new("TSStructure", g.Structure)
Group.new("TSInclude", g.Include)
Group.new("TSAnnotation", c.blue)
Group.new("TSAttribute", c.aqua)
Group.new("TSText", c.fg0, c.bg, b)
Group.new("TSStrong", c.fg0, c.bg, b)
Group.new("TSEmphasis", c.blue, nil, b)
Group.new("TSUnderline", c.blue, nil, b)
Group.new("TSTitle", c.cyan)
Group.new("TSLiteral", c.blue, nil, b)
Group.new("TSURI", c.cyan)
Group.new("TSVariable", c.fg0)
Group.new("TSVariableBuiltin", c.fg0, nil, i)

--TSAnnotation        
--TSAttribute         
--TSBoolean           
--TSCharacter         
--TSComment           
--TSConstructor       
--TSConditional       
--TSConstant          
--TSConstBuiltin      
--TSConstMacro        
--TSError             
--TSException         
--TSField             
--TSFloat             
--TSFunction          
--TSFuncBuiltin       
--TSFuncMacro         
--TSInclude           
--TSKeyword           
--TSKeywordFunction   
--TSLabel             
--TSMethod            
--TSNamespace         
--TSNone              
--TSNumber            
--TSOperator          
--TSParameter         
--TSParameterReference
--TSProperty          
--TSPunctDelimiter    
--TSPunctBracket      
--TSPunctSpecial      
--TSRepeat            
--TSString            
--TSStringRegex       
--TSStringEscape      
--TSTag               
--TSTagDelimiter      
--TSText              
--TSStrong            
--TSEmphasis          
--TSUnderline         
--TSStrike            
--TSTitle             
--TSLiteral           
--TSURI               
--TSType              
--TSTypeBuiltin       
--TSVariable          
--TSVariableBuiltin   
