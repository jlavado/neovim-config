local snippets = require 'snippets'

local sp = function(snippet)
	return (snippets.u.match_indentation(snippet));
end
local filename = "${=vim.api.nvim_buf_get_name(0):match(\"^.+/(.+)%..+$\")}"
local package = "package ${=string.gsub(tostring(vim.api.nvim_buf_get_name(0):match(\"java/(.+)/.+%..+$\")), \"/\",\".\")};\n\n"
local body = "{\n$0\n}"

snippets.snippets = {
	_global = {
		todo = "TODO: ";
		uname = function()
			return vim.loop.os_uname().sysname
		end;
		note = [[NOTE(${1=io.popen("id -un"):read"*l"}): ]];
	};
	java = {
		fin = sp(package .. "@FunctionalInterface\npublic interface " .. filename .. " " .. body),
		cla = sp(package .. "public class " .. filename .. " " .. body),
		ife = sp(package .. "public interface " .. filename .. " " .. body),
		enu = sp(package .. "public enum " .. filename .. " " .. body),
		val = sp(package .. "import lombok.Value;\n\n@Value\npublic class " .. filename .. " " .. body),
		exc = sp(package .. "public class " .. filename .. " extends Exception {\npublic " .. filename .. "(Exception e) {\nsuper(e);\n}\n}"),
		s = "String ",
		prs = sp("private String $0"),
		pv = sp("public void $1() " .. body),
		test = sp("@Test\npublic void $1() " .. body),
		m = sp("public $1 $2($3) " .. body),
		cs = sp("private static final String $1 = \"$1\";"),
		pf = sp("private final $0;"),
	}
}

snippets.use_suggested_mappings(true)

vim.api.nvim_set_keymap('i', '<c-s>', '<cmd>lua return require"snippets".expand_or_advance(1)<CR>', {noremap = true})
vim.api.nvim_set_keymap('i', '<c-j>', '<cmd>lua return require"snippets".advance_snippet(-1)<CR>', {noremap = true})
