local on_attach = function(client, bufnr)

    vim.api.nvim_buf_set_option(bufnr,'omnifunc', 'v:lua.vim.lsp.omnifunc')

    vim.fn.sign_define( 'DapBreakpoint', {text = '🛑', texthl = '', linehl = '', numhl = ''})
    vim.fn.sign_define( 'DapBreakpointCondition', {text = '❓', texthl = '', linehl = '', numhl = ''})

    local opts = {noremap = true, silent = true}
    vim.api.nvim_buf_set_keymap(bufnr,"n", "<A-CR>", "<Cmd>lua vim.lsp.buf.code_action()<CR>", opts)
    vim.api.nvim_buf_set_keymap(bufnr,'n', 'gD', '<Cmd>lua vim.lsp.buf.declaration()<CR>', opts)
    vim.api.nvim_buf_set_keymap(bufnr,'n', 'gd', '<Cmd>lua vim.lsp.buf.definition()<CR>', opts)
    vim.api.nvim_buf_set_keymap(bufnr,'n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
    vim.api.nvim_buf_set_keymap(bufnr,'n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
    vim.api.nvim_buf_set_keymap(bufnr,'n', 'K', '<Cmd>lua vim.lsp.buf.hover()<CR>', opts)
    vim.api.nvim_buf_set_keymap(bufnr,'n', '<space>f', '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)
    vim.api.nvim_buf_set_keymap(bufnr,'n', '<space>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
    vim.api.nvim_buf_set_keymap(bufnr,'n', '<space>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
    vim.api.nvim_buf_set_keymap(bufnr,'n', '<space>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
    vim.api.nvim_buf_set_keymap(bufnr,'n', '<space>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
    vim.api.nvim_buf_set_keymap(bufnr,'n', '<space>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
    vim.api.nvim_buf_set_keymap(bufnr,'n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
    vim.api.nvim_buf_set_keymap(bufnr,'n', '<space>e', '<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>', opts)
    vim.api.nvim_buf_set_keymap(bufnr,'n', '[d', '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', opts)
    vim.api.nvim_buf_set_keymap(bufnr,'n', ']d', '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>', opts)
    vim.api.nvim_buf_set_keymap(bufnr,'n', '<space>q', '<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>', opts)
    vim.api.nvim_buf_set_keymap(bufnr,'n', '<F5>', "<cmd>lua require'dap'.continue()<CR>", opts)
    vim.api.nvim_buf_set_keymap(bufnr,'n', '<F10>', "<cmd>lua require'dap'.step_over()<CR>", opts)
    vim.api.nvim_buf_set_keymap(bufnr,'n', '<F11>', "<cmd>lua require'dap'.step_into()<CR>", opts)
    vim.api.nvim_buf_set_keymap(bufnr,'n', '<F12>', "<cmd>lua require'dap'.step_out()<CR>", opts)
    vim.api.nvim_buf_set_keymap(bufnr,'n', '<leader>b', "<cmd>lua require'dap'.toggle_breakpoint()<CR>", opts)
    vim.api.nvim_buf_set_keymap(bufnr,'n', '<leader>B', "<cmd>lua require'dap'.set_breakpoint(vim.fn.input('Breakpoint condition: '))<CR>", opts)
    vim.api.nvim_buf_set_keymap(bufnr,'n', '<leader>lp', "<cmd>lua require'dap'.set_breakpoint(nil, nil, vim.fn.input('Log point message: '))<CR>", opts)
    vim.api.nvim_buf_set_keymap(bufnr,'n', '<leader>dr', "<cmd>lua require'dap'.repl.open()<CR>", opts)
    vim.api.nvim_buf_set_keymap(bufnr,'n', '<leader>dl', "<cmd>lua require'dap'.run_last()<CR>", opts)
end

return on_attach
