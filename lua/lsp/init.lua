require("trouble").setup()
require("dapui").setup()

vim.api.nvim_set_keymap("n", "<leader>xx", "<cmd>TroubleToggle<cr>", {silent = true, noremap = true})

vim.fn.sign_define("LspDiagnosticsSignError", {text = ""})
vim.fn.sign_define("LspDiagnosticsSignWarning", {text = ""})
vim.fn.sign_define("LspDiagnosticsSignInformation", {text = ""})
vim.fn.sign_define("LspDiagnosticsSignHint", {text = ""})

local lsp_installer = require("nvim-lsp-installer")
local lsp_on_attach = require("lsp.on_attach")

lsp_installer.on_server_ready(function(server)
    if server.name == "efm" then
        server:setup({
            filetypes = {"java"},
            on_attach = function(client)
                client.resolved_capabilities.document_formatting = false
            end
        })
    else if server.name == "sumneko_lua" then
        server:setup({ on_attach = lsp_on_attach,
        settings = {
            Lua = {
                diagnostics = {
                    globals = { 'vim' }
                }
            }
        }
    })
    else
        server:setup({ on_attach = lsp_on_attach})
    end end
end)
