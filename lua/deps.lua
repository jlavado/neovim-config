return require('packer').startup({
    function(use)
        use 'wbthomason/packer.nvim'
        use 'neovim/nvim-lspconfig'
        use 'mfussenegger/nvim-jdtls'
        use 'mfussenegger/nvim-dap'
        use 'nanotee/sqls.nvim'
        use 'tjdevries/colorbuddy.nvim'
        use 'kyazdani42/nvim-web-devicons'
        use 'kyazdani42/nvim-tree.lua'
        use 'nvim-lua/popup.nvim'
        use 'nvim-lua/plenary.nvim'
        use 'nvim-telescope/telescope.nvim'
        use {'nvim-telescope/telescope-fzf-native.nvim', run = 'make' }
        use 'tanvirtin/vgit.nvim'
        use 'norcalli/nvim-colorizer.lua'
        use 'folke/trouble.nvim'
        use 'glepnir/galaxyline.nvim'
        use 'npxbr/glow.nvim'
        use 'norcalli/snippets.nvim'
        use 'nvim-treesitter/nvim-treesitter'
        use 'nvim-treesitter/playground'
        use 'nvim-treesitter/nvim-treesitter-textobjects'
        use 'windwp/nvim-autopairs'
        use 'b3nj5m1n/kommentary'
        use 'hrsh7th/nvim-cmp'
        use 'hrsh7th/cmp-nvim-lsp'
        use 'hrsh7th/cmp-buffer'
        use 'hrsh7th/cmp-path'
        use 'ggandor/lightspeed.nvim'
        use 'weilbith/nvim-code-action-menu'
        use 'simrat39/symbols-outline.nvim'
        use "Pocco81/TrueZen.nvim"
        use 'vim-test/vim-test'
        use {"rcarriga/nvim-dap-ui", requires = {"mfussenegger/nvim-dap"}}
        use {'akinsho/bufferline.nvim', requires = 'kyazdani42/nvim-web-devicons'}
        use 'williamboman/nvim-lsp-installer'
    end
})
