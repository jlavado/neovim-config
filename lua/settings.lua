local indent = 4

vim.cmd 'syntax enable'
vim.cmd 'filetype plugin indent on'
vim.cmd 'se guifont=JetBrains\\ Mono\\ Nerd\\ Font:h18'
vim.cmd 'set cursorline'
vim.cmd 'set undofile'
vim.cmd 'set undodir=/home/joseluis.lavado/.tmp/nvim/undo'
vim.cmd 'set undolevels=1000'
vim.cmd 'set undoreload=10000'
vim.cmd 'set noswapfile'
vim.cmd 'set nowrap'
vim.cmd 'set shortmess+=Ic'
vim.cmd 'set completeopt-=preview'

vim.o.termguicolors = true
vim.o.expandtab = true
vim.o.shiftwidth = indent
vim.o.smartindent = true
vim.o.tabstop = indent
vim.o.hidden = true
vim.o.ignorecase = true
vim.o.scrolloff = 4
vim.o.shiftround = true
vim.o.smartcase = true
vim.o.number = true
vim.o.clipboard = 'unnamed,unnamedplus'
vim.o.backupdir = '/home/joseluis.lavado/.tmp'
vim.o.directory = '/home/joseluis.lavado/.tmp'
vim.o.encoding = 'UTF-8'

-- Highlight on yank
vim.cmd 'au TextYankPost * lua vim.highlight.on_yank {on_visual = false}'

