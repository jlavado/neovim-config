local bundles = {
	vim.fn.glob("/home/joseluis.lavado/Development/tools/java/java-debug/com.microsoft.java.debug.plugin/target/com.microsoft.java.debug.plugin-*.jar")
}
vim.list_extend(bundles, vim.split( vim.fn.glob("/home/joseluis.lavado/Development/tools/java/vscode-java-test/server/*.jar"), "\n"))

local config = {
	cmd = {
		'/home/joseluis.lavado/.sdkman/candidates/java/11.0.12-open/bin/java',
		'-Declipse.application=org.eclipse.jdt.ls.core.id1',
		'-Dosgi.bundles.defaultStartLevel=4',
		'-Declipse.product=org.eclipse.jdt.ls.core.product',
		'-Dlog.protocol=true',
		'-Dlog.level=ALL',
		'-Xms1g',
		'-Xmx2G',
		'-javaagent:/home/joseluis.lavado/Development/tools/java/lombok/lombok.jar',
		'--add-modules=ALL-SYSTEM',
		'--add-opens', 'java.base/java.util=ALL-UNNAMED',
		'--add-opens', 'java.base/java.lang=ALL-UNNAMED',
		'-jar', '/home/joseluis.lavado/Development/tools/java/jdtls/plugins/org.eclipse.equinox.launcher_1.6.400.v20210924-0641.jar',
		'-configuration', '/home/joseluis.lavado/Development/tools/java/jdtls/config_linux',
		'-data', '/home/joseluis.lavado/Development/.workspace/' .. vim.fn.fnamemodify( vim.fn.getcwd(), ':p:h:t'),
		'--add-modules=ALL-SYSTEM',
		'--add-opens', 'java.base/java.util=ALL-UNNAMED',
		'--add-opens', 'java.base/java.lang=ALL-UNNAMED',
	},
	root_dir = require('jdtls.setup').find_root({'.git'}),
	init_options = {
		bundles = bundles;
	},
    filetypes = {"java"},
    autostart = true,
    on_attach = function(client, bufnr)

        vim.api.nvim_buf_set_option(bufnr,'omnifunc', 'v:lua.vim.lsp.omnifunc')

        vim.fn.sign_define( 'DapBreakpoint', {text = '🛑', texthl = '', linehl = '', numhl = ''})
        vim.fn.sign_define( 'DapBreakpointCondition', {text = '❓', texthl = '', linehl = '', numhl = ''})

        local opts = {noremap = true, silent = true}
        vim.api.nvim_buf_set_keymap(bufnr,"n", "<A-o>", "<Cmd>lua require('jdtls').organize_imports()<CR>", opts)
        vim.api.nvim_buf_set_keymap(bufnr,"n", "<A-CR>", "<Cmd>lua require('jdtls').code_action()<CR>", opts)
        vim.api.nvim_buf_set_keymap(bufnr,"v", "<A-CR>", "<Cmd>lua require('jdtls').code_action(true)<CR>", opts)
        vim.api.nvim_buf_set_keymap(bufnr,"v", "<leader>r", "<Cmd>lua require('jdtls').code_action(true, 'refactor')<CR>", opts)
        vim.api.nvim_buf_set_keymap(bufnr,"n", "tm", "<Cmd>lua require('jdtls').test_nearest_method()<CR>", opts)
        vim.api.nvim_buf_set_keymap(bufnr,"n", "tc", "<Cmd>lua require('jdtls').test_class()<CR>", opts)

        vim.api.nvim_buf_set_keymap(bufnr,"n", "<leader>ev", "<Cmd>lua require('jdtls').extract_variable()<CR>", opts)
        vim.api.nvim_buf_set_keymap(bufnr,"v", "<leader>ev", "<Cmd>lua require('jdtls').extract_variable(true)<CR>", opts)
        vim.api.nvim_buf_set_keymap(bufnr,"n", "<leader>ec", "<Cmd>lua require('jdtls').extract_constant()<CR>", opts)
        vim.api.nvim_buf_set_keymap(bufnr,"v", "<leader>ec", "<Cmd>lua require('jdtls').extract_constant(true)<CR>", opts)
        vim.api.nvim_buf_set_keymap(bufnr,"v", "<leader>em", "<Cmd>lua require('jdtls').extract_method(true)<CR>", opts)

        vim.api.nvim_buf_set_keymap(bufnr,'n', 'gD', '<Cmd>lua vim.lsp.buf.declaration()<CR>', opts)
        vim.api.nvim_buf_set_keymap(bufnr,'n', 'gd', '<Cmd>lua vim.lsp.buf.definition()<CR>', opts)
        vim.api.nvim_buf_set_keymap(bufnr,'n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
        vim.api.nvim_buf_set_keymap(bufnr,'n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
        vim.api.nvim_buf_set_keymap(bufnr,'n', 'K', '<Cmd>lua vim.lsp.buf.hover()<CR>', opts)
        vim.api.nvim_buf_set_keymap(bufnr,'n', '<space>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
        vim.api.nvim_buf_set_keymap(bufnr,'n', '<space>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
        vim.api.nvim_buf_set_keymap(bufnr,'n', '<space>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
        vim.api.nvim_buf_set_keymap(bufnr,'n', '<space>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
        vim.api.nvim_buf_set_keymap(bufnr,'n', '<space>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
        vim.api.nvim_buf_set_keymap(bufnr,'n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
        vim.api.nvim_buf_set_keymap(bufnr,'n', '<space>e', '<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>', opts)
        vim.api.nvim_buf_set_keymap(bufnr,'n', '[d', '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', opts)
        vim.api.nvim_buf_set_keymap(bufnr,'n', ']d', '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>', opts)
        vim.api.nvim_buf_set_keymap(bufnr,'n', '<space>q', '<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>', opts)

        vim.api.nvim_buf_set_keymap(bufnr,'n', '<F5>', "<cmd>lua require'dap'.continue()<CR>", opts)
        vim.api.nvim_buf_set_keymap(bufnr,'n', '<F10>', "<cmd>lua require'dap'.step_over()<CR>", opts)
        vim.api.nvim_buf_set_keymap(bufnr,'n', '<F11>', "<cmd>lua require'dap'.step_into()<CR>", opts)
        vim.api.nvim_buf_set_keymap(bufnr,'n', '<F12>', "<cmd>lua require'dap'.step_out()<CR>", opts)
        vim.api.nvim_buf_set_keymap(bufnr,'n', '<leader>b', "<cmd>lua require'dap'.toggle_breakpoint()<CR>", opts)
        vim.api.nvim_buf_set_keymap(bufnr,'n', '<leader>B', "<cmd>lua require'dap'.set_breakpoint(vim.fn.input('Breakpoint condition: '))<CR>", opts)
        vim.api.nvim_buf_set_keymap(bufnr,'n', '<leader>lp', "<cmd>lua require'dap'.set_breakpoint(nil, nil, vim.fn.input('Log point message: '))<CR>", opts)
        vim.api.nvim_buf_set_keymap(bufnr,'n', '<leader>dr', "<cmd>lua require'dap'.repl.open()<CR>", opts)
        vim.api.nvim_buf_set_keymap(bufnr,'n', '<leader>dl', "<cmd>lua require'dap'.run_last()<CR>", opts)

        vim.api.nvim_buf_set_keymap(bufnr,'n', '<space>f', '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)

        require('jdtls.setup').add_commands()
        require('jdtls').setup_dap({hotcodereplace = 'auto'})
        require('jdtls.dap').setup_dap_main_class_configs()
    end
}

local finders = require'telescope.finders'
local sorters = require'telescope.sorters'
local actions = require'telescope.actions'
local pickers = require'telescope.pickers'
local action_state = require'telescope.actions.state'

require('jdtls.ui').pick_one_async = function(items, prompt, label_fn, cb)
  local opts = {}
  pickers.new(opts, {
    prompt_title = prompt,
    finder    = finders.new_table {
      results = items,
      entry_maker = function(entry)
        return {
          value = entry,
          display = label_fn(entry),
          ordinal = label_fn(entry),
        }
      end,
    },
    sorter = sorters.get_generic_fuzzy_sorter(),
    attach_mappings = function(prompt_bufnr)
      actions.select_default:replace(function()
        local selection = action_state.get_selected_entry(prompt_bufnr)

        actions.close(prompt_bufnr)

        cb(selection.value)
      end)

      return true
    end,
  }):find()
end

require('jdtls').start_or_attach(config)
